import { mock_getAllTreats } from '../test/mock-data'
global.window = {}
import 'mock-local-storage'
window.localStorage = global.localStorage


describe ('***This is my simple tests ***', ()=>{
    test('This verifies the getAllTreats function', ()=>{

        const ret = mock_getAllTreats
        expect.objectContaining({"treats":[{"id":0,"category":"Vitamin B1","desc":"Thiamin"},{"id":1,"category":"Vitamin B2","desc":"Riboflavin"},{"id":2,"category":"Vitamin B3","desc":"Niacinamide"}]}
        )
    })
})