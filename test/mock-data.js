const _data = {
    "treats" : [
        {
            "id": 0,
            "category": "Vitamin B1",
            "desc": "Thiamin"
        },
        {
            "id": 1,
            "category": "Vitamin B2",
            "desc": "Riboflavin"
        },
        {
            "id": 2,
            "category": "Vitamin B3",
            "desc": "Niacinamide"
        }
    ]
}
/////////////////////////////////////////////////////
if(!sessionStorage.getItem('loaded')){
    sessionStorage.setItem('treatsData', JSON.stringify(_data))
    sessionStorage.setItem('loaded',true)
}


export function mock_getAllTreats(){
    const strData = sessionStorage.getItem('treatsData')
    const objData = JSON.parse(strData)
    return objData;
}

export function mock_getTreat(id){
    const strData = sessionStorage.getItem('treatsData');
    const objData = JSON.parse(strData);

    const arrTreats = objData["treats"];

    const index = arrTreats.map(function(e) { return e.id; }).indexOf(id);
    if (index > -1) {
        return arrTreats[index]
    }
}


export function mock_createTreat(category, desc){
    const strData = sessionStorage.getItem('treatsData');
    const objData = JSON.parse(strData);

    const arrTreats = objData["treats"];
    const id = arrTreats.length;
    arrTreats[arrTreats.length] = {id, category, desc}

    sessionStorage.setItem('treatsData', JSON.stringify(objData))
}

export function mock_deleteTreat(id){
    const strData = sessionStorage.getItem('treatsData');
    const objData = JSON.parse(strData);

    const arrTreats = objData["treats"];

    const index = arrTreats.map(function(e) { return e.id; }).indexOf(id);
    if (index > -1) {
        arrTreats.splice(index,1);
    }

    sessionStorage.setItem('treatsData', JSON.stringify(objData))
}

export function mock_updateTreat(id,newCategoryVal,newDescriptionVal){

    const strData = sessionStorage.getItem('treatsData');
    const objData = JSON.parse(strData);

    const arrTreats = objData["treats"];

    const index = arrTreats.map(function(e) { return e.id; }).indexOf(id);
    if (index > -1) {
        arrTreats[index].category = newCategoryVal
        arrTreats[index].desc = newDescriptionVal
        sessionStorage.setItem('treatsData', JSON.stringify(objData))

    }


}
