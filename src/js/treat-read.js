import { mock_getAllTreats } from '../../test/mock-data.js'
import { deleteTreat } from './treat-delete.js'
import { updateTreat } from './treat-update.js'
window.deleteTreat = deleteTreat
window.updateTreat = updateTreat
////////////////////////////////////////////////////////////
function getCard(id, category, desc) {
    return`
    <div class="col-4 py-3">
    <div id = "${id}" class="card" style="width: 18rem;">     
        <div class="card-header text-end">
        <button type="button" class="btn-close" onclick="deleteTreat(${id})"></button>
        </div>
            <div class="card-body">
                <h5 class="card-title">${category}</h5>
                <p class="card-text">${desc}</p>
                <input class="btn btn-primary" type="button" value="Edit" onclick="window.updateTreat(${id})">
        </div>
    </div>`
}
////////////////////////////////////////////////////////////

export function readTreats(){

    const rowHtml = '<div id="dogTreatsRow" class="row justify-content-center"></div>'
    const domReadTreatsContainer = document.getElementById("dogTreatsRead")
    domReadTreatsContainer.innerHTML = rowHtml;

    const data = mock_getAllTreats()
    const treats = data["treats"];
    for(let i=0; i < treats.length; i++){
        const cardHtml = getCard(treats[i].id,treats[i].category,treats[i].desc)
        const rowDom = document.getElementById("dogTreatsRow")
        rowDom.innerHTML += cardHtml
    }
}
