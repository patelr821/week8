import { mock_getTreat, mock_updateTreat } from '../../test/mock-data.js'

function getUpdateModal(category, desc) {
    return`
    <div class="modal fade" id="updateTreatModal">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Update Treat</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form>
                <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Category:</label>
                <input type="text" class="form-control" id="treatCategory" value="${category}">
                </div>
                <div class="mb-3">
                <label for="message-text" class="col-form-label">Description:</label>
                <textarea class="form-control" id="treatDescription">${desc}"</textarea>
                </div>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Update</button>
            </div>
        </div>
        </div>
    </div>`
}


export function updateTreat(id){

    const treat = mock_getTreat(id)

    document.body.insertAdjacentHTML('afterbegin',getUpdateModal(treat.category, treat.desc))
    
    var myModalEl = document.getElementById('updateTreatModal')
    myModalEl.addEventListener('hidden.bs.modal', (event) => {
        
        const newCategoryVal = document.getElementById("treatCategory").value
        const newDescriptionVal = document.getElementById("treatDescription").value    
    
        if (newCategoryVal !== treat.category || newDescriptionVal !==treat.desc){
            mock_updateTreat(id,newCategoryVal,newDescriptionVal)
            location.reload()
        }
    })
    
    var myModal = new bootstrap.Modal(document.getElementById("updateTreatModal"), {})
    myModal.show()

}