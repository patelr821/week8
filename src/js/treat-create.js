import { mock_createTreat } from "../../test/mock-data.js";

export function onCreate() {
    const elCategory = document.getElementById("treatCategory");
    const elDesc = document.getElementById("treatDescription");
    mock_createTreat(elCategory.value, elDesc.value)
    elCategory.value = null;
    elDesc.value = null;

}